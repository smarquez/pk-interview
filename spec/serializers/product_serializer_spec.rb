require "rails_helper"
include BpmeHelper
include SerializerHelper

describe ProductSerializer do
  before :each do
    stub_sites_request
  end

  let(:object) { GetBpmeSites.all.sample.products.first }
  subject { serialize object }
  let(:attributes) { subject["data"]["attributes"] }

  it { expect(subject["data"]).to include("id" => object.id.to_s) }
  it { expect(attributes).to include("name" => object.name) }
  it { expect(attributes).to include("wetstock" => object.wetstock) }
  it { expect(attributes).not_to include("category") }
  it { expect(attributes).not_to include("code") }
  it { expect(attributes).not_to include("shortName") }
end
