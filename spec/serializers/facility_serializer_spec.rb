require "rails_helper"
include BpmeHelper
include SerializerHelper

describe FacilitySerializer do
  before :each do
    stub_sites_request
  end

  let(:object) { GetBpmeSites.all.sample.facilities.first }
  subject { serialize object }

  it { expect(subject["data"]).to include("id" => object.id.to_s) }
  it { expect(subject["data"]).not_to include("name") }
  it { expect(subject["data"]).not_to include("created") }
  it { expect(subject["data"]).not_to include("updated") }
end
