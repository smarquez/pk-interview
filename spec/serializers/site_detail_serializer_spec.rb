require "rails_helper"
include BpmeHelper
include SerializerHelper

describe SiteDetailSerializer do
  before :each do
    stub_sites_request
  end

  let(:object) { GetBpmeSites.all.sample }

  describe "serialization" do
    subject { serialize object }
    let(:attributes) { subject["data"]["attributes"] }

    it { expect(subject["data"]).to include("id" => object.id.to_s) }
    it { expect(attributes).to include("city" => object.city) }
    it { expect(attributes).to include("latitude" => object.latitude) }
    it { expect(attributes).to include("longitude" => object.longitude) }
    it { expect(attributes).to include("phone" => object.phone) }
    it do
      expect(attributes).to(
        include("site-display-name" => object.siteDisplayName)
      )
    end
    it { expect(attributes).to include("street" => object.street) }
    it do
      expect(attributes).to include("street-number" => object.streetNumber.to_s)
    end
    it { expect(subject["data"]["relationships"]).to include("facilities") }
    it { expect(subject["data"]["relationships"]).to include("products") }

    it { expect(attributes).not_to include("contact-name") }
    it { expect(attributes).not_to include("fax") }
    it { expect(attributes).not_to include("name") }
    it { expect(attributes).not_to include("post-code") }
    it { expect(attributes).not_to include("status") }
    it { expect(attributes).not_to include("time-zone-id") }
    it { expect(attributes).not_to include("suburb") }
    it { expect(subject["data"]["relationships"]).not_to include("ownership") }
    it { expect(subject["data"]["relationships"]).not_to include("schedules") }
  end

  describe "#today_schedule" do
    subject { described_class.new object }
    it "returns today's schedule in site's timezone" do
      day_of_week = subject.today_schedule.dayOfWeek
      dow_in_tz = Date.today.in_time_zone(subject.object.timeZoneId).wday
      expect(day_of_week).to eq dow_in_tz
    end
  end
end
