require "rails_helper"
include BpmeHelper
include SerializerHelper

describe OwnershipSerializer do
  before :each do
    stub_sites_request
  end

  let(:object) { GetBpmeSites.all.sample.ownership }
  subject { serialize object }

  it { expect(subject["data"]).to include("id" => object.id.to_s) }
  it { expect(subject["data"]).not_to include("name") }
end
