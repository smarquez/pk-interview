require "rails_helper"
include BpmeHelper

describe "Sites" do
  before :each do
    stub_sites_request
  end

  let(:sites) { GetBpmeSites.all }
  let(:site) { sites.sample }

  it "sends a list of sites" do
    get v1_sites_url
    parsed_response = JSON.parse(response.body)["data"]

    expect(response).to be_success
    expect(parsed_response).to be_kind_of Array
    expect(parsed_response.length).to eq(sites.count)
  end

  it "sends a specific site" do
    get v1_site_url(site.id)
    parsed_response = JSON.parse(response.body)["data"]

    expect(response).to be_success
    expect(parsed_response).to be_kind_of Hash
    expect(parsed_response["id"].to_i).to eq(site.id)
  end
end
