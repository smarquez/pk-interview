require "rails_helper"
include BpmeHelper

describe GetBpmeSites do
  before :each do
    stub_sites_request
  end

  describe "all" do
    let(:subject) { GetBpmeSites.all }

    it "returns a list of sites" do
      expect(subject.count).to eq(30)
    end

    it "instances results into Site objects" do
      GetBpmeSites.all.each do |site|
        expect(site).to be_kind_of Site
      end
    end
  end

  describe "find" do
    let(:subject) { GetBpmeSites.all.sample }

    it "returns a specific site" do
      expect(subject).to eq(subject)
    end
  end
end
