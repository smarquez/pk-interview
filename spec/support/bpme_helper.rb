module BpmeHelper
  def stub_sites_request
    sites_fixture = File.join "spec", "fixtures", "sites.json"
    stub_request(:get, GetBpmeSites::URL).
      to_return body: File.read(sites_fixture)
  end
end
