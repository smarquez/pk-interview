module SerializerHelper
  def serialize(object)
    serializer = described_class.new(object)
    serialization = ActiveModelSerializers::Adapter.create(serializer)
    JSON.parse(serialization.to_json)
  end
end
