require "json"
require "rest-client"

class GetBpmeSites
  URL =
    "https://npi6i9hbhh.execute-api.ap-southeast-2.amazonaws.com/v1/sites".
    freeze

  def self.all
    fetch_sites["content"].map do |site|
      Site.new site
    end
  end

  def self.find(id)
    site_attrs = fetch_sites["content"].detect { |site| site["id"] == id }
    Site.new site_attrs
  end

  def self.fetch_sites
    Rails.cache.fetch("bpme_sites", expires_in: 15.minutes) do
      JSON.parse RestClient.get(URL)
    end
  end

  private_class_method :fetch_sites
end
