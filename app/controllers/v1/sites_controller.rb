class V1::SitesController < ApplicationController
  def index
    @sites = GetBpmeSites.all

    render json: @sites
  end

  def show
    @site = GetBpmeSites.find params[:id].to_i

    render json: @site, serializer: SiteDetailSerializer
  end
end
