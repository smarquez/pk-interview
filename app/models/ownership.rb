class Ownership < ActiveModelSerializers::Model
  alias :read_attribute_for_serialization :send

  attr_accessor :id, :name
end
