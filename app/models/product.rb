class Product < ActiveModelSerializers::Model
  alias :read_attribute_for_serialization :send

  attr_accessor :category, :code, :name, :shortName, :wetstock
end
