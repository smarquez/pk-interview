class Site < ActiveModelSerializers::Model
  alias :read_attribute_for_serialization :send

  attr_accessor :id, :city, :contactName, :fax, :latitude, :longitude, :name,
                :phone, :postCode, :province, :timeZoneId, :siteDisplayName,
                :status, :street, :streetNumber, :suburb, :facilities,
                :ownership, :products, :schedules

  def initialize(attrs = {})
    super
    @ownership = Ownership.new attrs["ownership"]
    @facilities = init_has_many_relation "facilities"
    @products = init_has_many_relation "products"
    @schedules = init_has_many_relation "schedules"
  end

  private

  def init_has_many_relation(relation)
    attributes[relation].map do |rel_attrs|
      relation.classify.constantize.new rel_attrs
    end
  end
end
