class Schedule < ActiveModelSerializers::Model
  alias :read_attribute_for_serialization :send

  attr_accessor :id, :dayOfWeek, :opened, :closed
end
