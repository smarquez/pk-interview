class SiteDetailSerializer < ActiveModel::Serializer
  cache key: "site_detail", except: [:opened, :closed]

  attributes :id, :city, :latitude, :longitude, :phone, :siteDisplayName,
             :street, :streetNumber, :opened, :closed

  has_many :facilities
  has_many :products

  def today_schedule
    dow_in_tz = Date.today.in_time_zone(object.timeZoneId).wday
    object.schedules.detect { |s| s.dayOfWeek.to_i == dow_in_tz }
  end

  def opened
    today_schedule.opened
  end

  def closed
    today_schedule.closed
  end
end
