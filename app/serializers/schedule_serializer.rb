class ScheduleSerializer < ActiveModel::Serializer
  attributes :id, :opened, :closed
end
