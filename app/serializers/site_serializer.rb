class SiteSerializer < ActiveModel::Serializer
  cache key: "site"

  attributes :id, :city, :latitude, :longitude, :phone, :siteDisplayName,
             :street, :streetNumber

  belongs_to :ownership
end
