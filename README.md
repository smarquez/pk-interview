# Backend Candidate Challenge

After receiving the design signoff from the client we are now ready to build the next stage of the BPMe app, Find a BP. We have been provided the following 3 sates from our design team and need to implement an API for our mobile teams (Android & iOS) to consume so that they can get underway.

To do this, we first need to consume the Site data from BP’s systems. We’ve been provided with the following URL and brief documentation to go with it. BP have said we can’t call this more frequently than every 15 minutes because their system can’t handle the load.

The URL is: https://npi6i9hbhh.execute-api.ap-southeast-2.amazonaws.com/v1/sites

When creating our API for the Mobile team, it’s worth thinking about the following things:
- Where business logic should be (client vs server)
- How can we ensure the end user has the best, quickest experience possible
- How can we make sure that what we are returning via our API fits the spec we give to our Mobile Devs

## Screen Descriptions

These screen descriptions might help you with understanding what the problem is, and how you might solve it.

**State 1:**

<img width="283" alt="screen shot 2016-09-16 at 10 31 01 am" src="https://cloud.githubusercontent.com/assets/693929/18573800/292df804-7c1b-11e6-9743-88edc1bfe1a1.png">

This screen simply shows the pins on the map with their associated icons. Don’t worry about the site filters up the top, those will be completed another day.

**State 2:**

<img width="282" alt="screen shot 2016-09-16 at 10 31 35 am" src="https://cloud.githubusercontent.com/assets/693929/18573801/2a5a80ee-7c1b-11e6-8ad5-c73be29ea1ef.png">

When tapping on the icons, the app is going to present this very limited information. It would be great if the phone didn’t have to do another request to our API to get this information, as we don’t want to add any more latency to the end user. Don’t worry about the routing on the roads, as this will be done by the device itself.

**State 3:**

<img width="285" alt="screen shot 2016-09-16 at 10 31 51 am" src="https://cloud.githubusercontent.com/assets/693929/18573802/2b9ff6d2-7c1b-11e6-905b-14ae8f8af106.png">

This screen is where the remaining site data is shown. The design team would like the Fuel products to be shown as icons. We’ve been told by BP that a Product with the value "wetstock": true is a Fuel Product.

