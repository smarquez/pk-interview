Rails.application.routes.draw do
  namespace "v1" do
    resources "sites", only: [:index, :show]
  end
end
